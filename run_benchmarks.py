#!/usr/bin/python
import shlex, subprocess, string, time, os, errno, shutil, mmap, multiprocessing
############### Generic procedure to run benchmarks with arguments ##############################
def RunBNCH(__name, __cmd, __prm=None, __defpath=None, __pipe=False, __ext=".rtf", __output=True):
   #Local variable assignment
   _timestr = time.strftime("%Y%m%d-%H%M%S")
   _logdir = "/tmp/BenchResult/"
   _filepath = _logdir+__name+"@"+_timestr+__ext
   _space=" "
   _output=None
   
   try:
      #Check the result folder and make proper command parameters to run
      if not os.path.exists(_logdir): os.makedirs(_logdir)
      if __output==True: _output = open(_filepath, "w")
      if (__defpath<>None and os.path.exists(__defpath)): os.chdir(__defpath)
      if __prm==None: _space=None
      if __pipe==True: __cmd=__cmd+" 2> "+_filepath
      print "\033[41m\nExecuting \""+str(__cmd)+"\" from \""+os.getcwd()+"\"...\033[0m\n"
      subprocess.check_call(__cmd, stdout=_output, shell=True)

   except subprocess.CalledProcessError as e:
      print "\033[33m\n"+str(e)+"\033[0m\n"

   else:
      print "\033[42m\nExection has been finished." 
      print "\033[42mThe output would be available at: \""+_filepath+"\"...\033[0m\n\n\n"
   
   if __output==True: _output.close()
   return
############### Main procedure to initiate benchmarks with different parameters ################
try:
   #1-Collecte hardware information
   RunBNCH("HardInfo", "hardinfo -r -f text")

   #2-Generate both the basic system index and the graphics index
   #RunBNCH("UnixBench", "./Run gindex", __defpath="/tmp/UnixBench")
##   RunBNCH("UnixBench", "./Run", __defpath="/tmp/UnixBench")

   #4-Simulate I/O calls from 100 simultaneous users for 10 minutes
   RunBNCH("DBench", "dbench 50")

   #5-Measure disk I/O latency (IOPS)
   RunBNCH("IOPingIPOS", "ioping -D -c 600 .")
   
   #6-Measure disk sequential speed (DTR).
   RunBNCH("IOPingDTR", "ioping -RL .")

   #7-Write 1Megabyte for 4096 times (4GB file size). Typical DTR test.
   RunBNCH("DD", "dd if=/dev/zero of=/tmp/io-test bs=1M count=4K conv=fdatasync", __pipe=True)

   #8-Create a 1GB file, and perform 4KB reads and writes using a 75%/25% (3 reads for every 1 write) split within the file.
   #  With 16 operations running at a time. The 3:1 ratio is a rough approximation of a typical database.
   RunBNCH("FIORW","fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=/tmp/io-test --bs=4k --iodepth=16 --size=1G --readwrite=randrw --rwmixread=75")
   RunBNCH("FIORW","rm /tmp/io-test", __output=False)

   #9-Benchmark CPU performance on multithreaded CPUs
   #  Assigned _thread_num=1 as t2.micro, 2 as t2.medium & D2v2 Std and 4 as c3.xlarge
   _thread_num=1
   RunBNCH("SYSBenchCPU", "sysbench --test=cpu --cpu-max-prime=20000 --num-threads="+str(_thread_num)+" run")

   #10-Measure file I/O performance by creating 64 files, 4K each (default cluster size) and 
   #   performing random read, random write and combined random read/write
   _sysbench_cmd="sysbench --test=fileio --file-num=64 --file-block-size=4K --file-total-size=8GB "
   _sysbench_run=" --init-rng=on --max-time=240 --max-requests=0 --num-threads="+str(2*_thread_num)+" run"
   #10-1: Prepare test files before run
   RunBNCH("SYSBench", _sysbench_cmd+"prepare", __output=False)
   #10-2: Random Read only 
   RunBNCH("SYSBenchRD", _sysbench_cmd+"--file-test-mode=rndrd"+_sysbench_run)
   #10-3: Random Write using default fsync behavior (Linux will decide to flush data from memory to disk)
   RunBNCH("SYSBenchWR", _sysbench_cmd+"--file-test-mode=rndwr"+_sysbench_run)
   #10-4: Mixed Random Read and Write
   RunBNCH("SYSBenchRW", _sysbench_cmd+"--file-test-mode=rndrw"+_sysbench_run)
   #10-5: Clean-up prepared test file after run
   RunBNCH("SYSBench", _sysbench_cmd+"cleanup", __output=False)

   #11-Mesure memory read and write performance
   RunBNCH("SYSBenchRAMR", "sysbench --test=memory --memory-block-size=1K --memory-scope=global --memory-total-size=10G --memory-oper=read run")
   RunBNCH("SYSBenchRAMW", "sysbench --test=memory --memory-block-size=1K --memory-scope=global --memory-total-size=10G --memory-oper=write run")

   #12-Redis

   #13-IPerf
except Exception,e:
   print str(e)


#   for p in xrange(20000,200000,20000):

