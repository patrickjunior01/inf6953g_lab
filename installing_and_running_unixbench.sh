#Updates fetching and Installation
echo -e '\E[39;46m'"\033[1mInstalling Updates...\033[0m"

sudo apt-get -y update
sudo apt-get -y upgrade

#UnixBench Installation
echo -e '\E[37;44m'"\033[1m\nInstalling UnixBench...\033[0m"
sudo apt-get -y install libx11-dev libgl1-mesa-dev libxext-dev perl perl-modules build-essential make
sudo apt-get -y install gcc
wget -N http://byte-unixbench.googlecode.com/files/UnixBench5.1.3.tgz
tar -xvf UnixBench5.1.3.tgz 
cd UnixBench
sudo make
cd /home/ubuntu

echo -e '\E[34;43m'"\033[1m\nExecuting UnixBench.\033[0m"
python3 UnixBench.py
