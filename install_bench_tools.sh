#!/bin/bash
tput clear

#Updates fetching and Installation
echo -e '\E[39;46m'"\033[1mInstalling Updates...\033[0m"
apt-get -y update
apt-get -y dist-upgrade

#UnixBench Installation
#echo -e '\E[37;44m'"\033[1m\nInstalling UnixBench...\033[0m"
#apt-get -y install libx11-dev libgl1-mesa-dev libxext-dev perl perl-modules build-essential make
#wget -N http://byte-unixbench.googlecode.com/files/UnixBench5.1.3.tgz
#tar -xvf UnixBench5.1.3.tgz -C /tmp/

#IOPing Installation
#echo -e '\E[37;44m'"\033[1m\nInstalling IOPing...\033[0m"
apt-get -y install ioping

#Redis Installation
#echo -e '\E[37;44m'"\033[1m\nInstalling Redis...\033[0m"
#wget -N http://download.redis.io/releases/redis-2.8.19.tar.gz
#tar -xzf redis-2.8.19.tar.gz
#mv redis-2.8.19 /etc
#cd /etc/redis-2.8.19
#make distclean
#make
#apt-get -y install tcl8.5 dejagnu 
#make install
#cd utils ./install_server.sh

#DBench, SysBench, HardInfo, fio, gtkperf and Phoronix Test Suite Installation
echo -e '\E[37;44m'"\033[1m\nInstalling IOPing, DBench, SysBench, HardInfo, FIO, GTKPerf & Phoronix...\033[0m"
apt-get -y install libx11-dev libgl1-mesa-dev libxext-dev perl perl-modules build-essential make
apt-get -y install ioping
apt-get -y install dbench sysbench hardinfo fio gtkperf phoronix-test-suite

echo -e '\E[34;43m'"\033[1m\nInstalling has been completed.\033[0m"

