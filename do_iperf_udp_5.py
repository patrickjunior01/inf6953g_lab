############ SCRIPT POUR LE LANCEMENT IPERF udp ############ 

import subprocess
import sys
import csv
import xlsxwriter
import getpass

login=getpass.getuser() ## To get the current user's username

fichier_graph_excel = "/home/"+login+"/instance_udp_medium.xlsx" # fichier excel qui contient le graphe des resultats
fichier_resultat_moyen= "/home/"+login+"/resultat_t2_iperf_udp_medium.txt" #fichier qui contient les resultats finaux et la moyenne
fichier_enregistrement_donnee = "/home/"+login+"/output_t2_iperf_udp_medium.txt"

workbook = xlsxwriter.Workbook(fichier_graph_excel)

val = 5  # nombre d'execution a faire

som_bw = 0  # variable qui consiste a additionner les valeurs de sender
som_jitter = 0  # variable qui consiste a additionner les valeurs de sender
som_transfer = 0  # variable qui consiste a additionner les valeurs de sender

moy_bw = 0  # variable qui consiste a faire la moyenne de sender
moy_jitter = 0  # variable qui consiste a faire la moyenne de sender
moy_transfer = 0  # variable qui consiste a faire la moyenne de sender



####################################################################################
#La boucle For permet de faire 5 execution sur une instance
for k in range(val):
	list_temp=[] # variable qui contient les differents valeur de la duree
	list_bw=[] # variable qui content les differents valeur de la BandWith
	list_sender=[] # variable qui content les differents valeur de sender
	list_receiver=[] # variable qui content les differents valeur de receiver
	
	####################################################################################
	#Execute Iperf qui va enregistrer tous ses executions dans un fichier output_t2_micro_iperf_udp.txt
	
	cmdiperf = "sudo iperf3 -c 172.31.24.112 -t 90 -p 80 -u -b 250m"
	f = open(fichier_enregistrement_donnee,'wr')
	sys.stdout = f
	subprocess.check_call(cmdiperf, shell=True, stdout=f) #Execution of iperf3
	sys.stdout = sys.__stdout__
	f.close()
	print "############ The execution of iperf3 is now terminated ############"
	####################################################################################
	
	####################################################################################
	# Enregistrement des resultats dans un fichier texte , les donnees dans un fichier excel
	
	# creation d'une feuille dans un fichier excel
	worksheet = workbook.add_worksheet("Execution"+str(k))
	
	# Start from the first cell. Rows and columns are zero indexed.
	
	worksheet.write(0, 0, "duree")
	worksheet.write(0, 1,"BandWith")
	
	
	f =open(fichier_enregistrement_donnee,"r")
	f1 = open(fichier_resultat_moyen,'a') # fichier qui contient les resultats de tous les executions
	
	
	for line in f:
		#print(line)
		
		tempList = line.split();
		#print (tempList)
		####################################################################################
		#Consiste a retirer tous les lignes se terminant par Bytes et existe dans le fichier output_t2_micro_iperf_udp.txt seront mis dans un list qui
		# pour role d'ajouter ses contenus dans un fichier excel instance_udp.xlsx
		# dans la ligne il recupere deux valeurs qui sont la duree, BandWith
		
		
		
		#if (len(tempList) < 8):
		#	continue
		
		if (len(tempList) >= 8):
			if (len(tempList)!=0 and str(tempList[7]) =="Mbits/sec"):
				secondList = tempList[2].split('-');
				secondList[0]= secondList[0].replace(',','.');
				tempList[6]= tempList[6].replace(',','.');
				list_temp.append(secondList[0])
				list_bw.append(float(tempList[6]))
		####################################################################################
	
		####################################################################################
		# consiste a recuperer les resultats finaux de chaque execution et de l'ecrire dans un fichier texte
		
		if (len(tempList) >= 7):
			if (len(tempList)!=0 and str(tempList[6]) =="Lost/Total"):
				f1.write("Debut Execution"+str(k))
				f1.write("\n\n")
				for j in tempList:
					f1.write(j+" ")
				f1.write("\n")
	
		if (len(tempList) >= 11):
			if (len(tempList)!=0 and str(tempList[9]) =="ms"):
				for j in tempList:
					f1.write(j+" ")
				#list_sender.append(tempList[6])
				tempList[6]= tempList[6].replace(',','.');
				som_bw = som_bw + float(tempList[6])
			
				tempList[4]= tempList[4].replace(',','.');
				som_transfer = som_transfer + float(tempList[4])
			
				tempList[8]= tempList[8].replace(',','.');
				som_jitter = som_jitter + float(tempList[8])
				f1.write("\n")
				f1.write("\n")
		####################################################################################
	
	####################################################################################
	#Consiste a ajouter les differents valeurs de duree et bandwith dans un fichie excel
	worksheet.write_column('A2', list_temp)
	worksheet.write_column('B2', list_bw)
	####################################################################################
	
	
	####################################################################################
	# consiste a dessiner dans un fichier excel les differents valeurs qui se trouve dans chaque feuille
	
	# Create a new chart object. In this case an embedded chart.
	chart1 = workbook.add_chart({'type': 'line'})

	# Configure the first series.
	
	chart1.add_series({
		'name':       '=Execution0!$B$1',
		'categories': '=Execution0!$A$2:$A$'+str(len(list_bw)),
		'values':     '=Execution0!$B$2:$B$'+str(len(list_bw)),})

	# Add a chart title and some axis labels.
	chart1.set_title ({'name': 'Results of sample analysis UDP'})
	chart1.set_x_axis({'name': 'duree en sec'})
	chart1.set_y_axis({'name': 'BandWith en Mbits/sec'})

	# Set an Excel chart style. Colors with white outline and shadow.
	chart1.set_style(10)

	# Insert the chart into the worksheet (with an offset).
	worksheet.insert_chart('D2', chart1, {'x_offset': 25, 'y_offset': 10})
	####################################################################################
	
	####################################################################################
	
print ("somme transfer "+str(som_transfer))
moy_transfer = som_transfer / val
print ("moyenne transfer "+str(moy_transfer))

print ("somme Bande passante "+str(som_bw))
moy_bw = som_bw / val
print ("moyenne Bande passante "+str(moy_bw))

print ("somme jitter "+str(som_jitter))
moy_jitter = som_jitter / val
print ("moyenne jitter: "+str(moy_jitter))


####################################################################################
# Enregistrement des des moyennes de la bande passante et des jitter et transfer de donnee

f1.write("Moyenne generale pour "+str(val)+" execution\n\n")	
f1.write("moyenne de transfer de donnee en MBytes: "+str(moy_transfer))
f1.write("\n")
f1.write("moyenne de la bande passante en MBits: "+str(moy_bw))
f1.write("\n")
f1.write("moyenne de Jitter en ms: "+str(moy_jitter))
####################################################################################

workbook.close()
####################################################################################