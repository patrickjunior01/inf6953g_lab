#Updates fetching and Installation
echo -e '\E[39;46m'"\033[1mInstalling Updates...\033[0m"

sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install build-essential
sudo apt-get -y install tcl8.5

#UnixBench Installation
echo -e '\E[37;44m'"\033[1m\nInstalling Redis...\033[0m"
sudo apt-get -y install redis-server
echo -e '\E[34;43m'"\033[1m\nExecuting Redis.\033[0m"
python3 Redis.py
#cat /proc/cpuinfo > cpuinfo.txt && cat /proc/meminfo > meminfo.txt