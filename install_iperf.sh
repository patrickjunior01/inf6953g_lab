#!/bin/bash
#To install iperf3 on Debian/Ubuntu hosts:
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install git gcc make
    sudo git clone https://github.com/esnet/iperf
    cd iperf
    sudo ./configure
    sudo make
    sudo make install
    sudo ldconfig