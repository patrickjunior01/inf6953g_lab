############ SCRIPT POUR LE LANCEMENT IPERF tcp ############ 

import subprocess
import sys
import csv
import xlsxwriter
import getpass


login=getpass.getuser() ## To get the current user's username
fichier_graph_excel = "/home/"+login+"/instance_medium.xlsx" # fichier excel qui contient le graphe des resultats
fichier_resultat_moyen= "/home/"+login+"/resultat_t2_iperf_medium.txt" #fichier qui contient les resultats finaux et la moyenne
fichier_enregistrement_donnee = "/home/"+login+"/output_t2_iperf_medium.txt" 

workbook = xlsxwriter.Workbook(fichier_graph_excel)

val = 5  # vnombre d'execution a faire
som_sender = 0  # variable qui consiste a additionner les valeurs de sender
som_receiver = 0 # variable qui consiste a additionner les valeurs de receiver
moy_sender = 0  # variable qui consiste a faire la moyenne de sender
moy_receiver = 0  # variable qui consiste a faire la moyenne de receiver
som_trans_donn_receiver =0 # variable qui consiste a additionner les differents valeurs de des donnee tranfere pendant un temps
som_trans_donn_sender =0 # variable qui consiste a additionner les differents valeurs de des donnee tranfere pendant un temps
moy_trans_donn_receiver =0 # variable qui consiste a faire la moyenne des differents valeurs de des donnee tranfere pendant un temps
moy_trans_donn_receiver =0 # variable qui consiste a faire la moyenne des differents valeurs de des donnee tranfere pendant un temps


####################################################################################
#La boucle For permet de faire 5 execution sur une instance
for k in range(val):

	list_temp=[] # variable qui contient les differents valeur de la duree
	list_bw=[] # variable qui content les differents valeur de la BandWith
	list_sender=[] # variable qui content les differents valeur de sender
	list_receiver=[] # variable qui content les differents valeur de receiver
	
	####################################################################################
	#Execute Iperf qui va enregistrer tous ses executions dans un fichier texte
	
	cmdiperf = "sudo iperf3 -c 172.31.24.112 -i 1 -t 90 -f MBytes -V -p 80"
	f = open(fichier_enregistrement_donnee,'wr')
	sys.stdout = f
	subprocess.check_call(cmdiperf, shell=True, stdout=f) #Execution of iperf3
	sys.stdout = sys.__stdout__
	f.close()
	print "############ The execution of iperf3 is now terminated ############"
	####################################################################################
	
	####################################################################################
	# Enregistrement des resultats dans un fichier texte , les donnees dans un fichier excel
	
	# creation d'une feuille dans un fichier excel
	worksheet = workbook.add_worksheet("Execution"+str(k))
	
	# Start from the first cell. Rows and columns are zero indexed.
	
	worksheet.write(0, 0, "duree")
	worksheet.write(0, 1,"BandWith")
	
	
	f =open(fichier_enregistrement_donnee,"r")
	f1 = open(fichier_resultat_moyen,'a') # fichier qui contient les resultats de tous les executions
	
	
	for line in f:
	
		tempList = line.split();
	
		####################################################################################
		#Consiste a retirer tous les lignes se terminant par Bytes et existe dans le fichier output_t2_micro_iperf.txt seront mis dans un list qui
		# pour role d'ajouter ses contenus dans un fichier excel instance.xlsx
		# dans la ligne il recupere deux valeurs qui sont la duree et BandWith
		
		if (len(tempList)!=0 and "Bytes" in str(tempList[len(tempList)-1]) =="KBytes"):
			secondList = tempList[2].split('-');
			secondList[0]= secondList[0].replace(',','.');
			tempList[6]= tempList[6].replace(',','.');
			list_temp.append(secondList[0])
			list_bw.append(float(tempList[6]))
		####################################################################################
		
		####################################################################################
		# consiste a recuperer les resultats finaux de chaque execution et de l'ecrire dans un fichier texte
		
		if (len(tempList)!=0 and str(tempList[0]) =="Test"):
			f1.write("Debut Execution"+str(k))
			f1.write("\n\n")
			for j in tempList:
				f1.write(j+" ")
			f1.write("\n")
	
		if (len(tempList)!=0 and str(tempList[len(tempList)-1]) =="Retr"):
			for j in tempList:
				f1.write(j+" ")
			f1.write("\n")
	
		if (len(tempList)!=0 and str(tempList[len(tempList)-1]) =="sender"):
			for j in tempList:
				f1.write(j+" ")
				
			####################################################	
			# Ici on recupere la bandwith
			list_sender.append(tempList[6])
			tempList[6]= tempList[6].replace(',','.');
			som_sender = som_sender + float(tempList[6])
			####################################################
			
			####################################################	
			# Ici on recupere les donnees transfere
			list_receiver.append(tempList[4]) 
			tempList[4]= tempList[4].replace(',','.');
			som_trans_donn_sender = som_trans_donn_sender + float(tempList[4])
			####################################################
	
			f1.write("\n")
			
			
		if (len(tempList)!=0 and str(tempList[len(tempList)-1]) =="receiver"):
			for j in tempList:
				f1.write(j+" ")
				
			####################################################	
			# Ici on recupere la bandwith
			list_receiver.append(tempList[6])
			tempList[6]= tempList[6].replace(',','.');
			som_receiver = som_receiver + float(tempList[6])
			####################################################
			
			####################################################	
			# Ici on recupere les donnees transfere
			list_receiver.append(tempList[4]) 
			tempList[4]= tempList[4].replace(',','.');
			som_trans_donn_receiver = som_trans_donn_receiver + float(tempList[4])
			####################################################
			
			f1.write("\n")
			
	
		if (len(tempList)!=0 and str(tempList[0]) =="CPU"):
			for j in tempList:
				f1.write(j+" ")
			f1.write("\n")
			f1.write("\n")
		####################################################################################
	
	####################################################################################
	#Consiste a ajouter les differents valeurs de duree et bandwith dans un fichier excel
	worksheet.write_column('A2', list_temp)
	worksheet.write_column('B2', list_bw)
	####################################################################################
	
	
	####################################################################################
	# consiste a dessiner dans un fichier excel les differents valeurs qui se trouve dans chaque feuille
	
	# Create a new chart object. In this case an embedded chart.
	chart1 = workbook.add_chart({'type': 'line'})

	# Configure the first series.
	
	chart1.add_series({
		'name':       '=Execution0!$B$1',
		'categories': '=Execution0!$A$2:$A$'+str(len(list_bw)),
		'values':     '=Execution0!$B$2:$B$'+str(len(list_temp)),})

	# Add a chart title and some axis labels.
	chart1.set_title ({'name': 'Results of sample analysis'})
	chart1.set_x_axis({'name': 'duree'})
	chart1.set_y_axis({'name': 'BandWith'})

	# Set an Excel chart style. Colors with white outline and shadow.
	chart1.set_style(10)

	# Insert the chart into the worksheet (with an offset).
	worksheet.insert_chart('D2', chart1, {'x_offset': 25, 'y_offset': 10})
	####################################################################################
	
	####################################################################################
	
print ("somme de bande passante sender "+str(som_sender))
moy_sender = som_sender / val
print ("moyenne de bande passante sender "+str(moy_sender))

print ("somme de bande passante receiver "+str(som_receiver))
moy_receiver = som_receiver / val
print ("moyenne de bande passante receiver "+str(moy_receiver))

print ("somme de donnee transfere receiver"+str(som_trans_donn_receiver))
moy_trans_donn_receiver = som_trans_donn_receiver / val
print ("moyenne de donnee transfere receiver"+str(moy_trans_donn_receiver))

print ("somme de donnee transfere sender"+str(som_trans_donn_sender))
moy_trans_donn_sender = som_trans_donn_sender / val
print ("moyenne de donnee transfere receiver"+str(moy_trans_donn_sender))


####################################################################################
# Enregistrement des des moyennes de la bande passante et des donnees transfere pour le receiver et sender

f1.write("Moyenne generale pour "+str(val)+" execution\n\n")	
f1.write("moyenne de la bande passant sender en MBytes: "+str(moy_sender))
f1.write("\n")
f1.write("moyenne de la bande passante receiver en MBytes: "+str(moy_receiver))
f1.write("\n")
f1.write("moyenne de donnee transfere sender en MBytes: "+str(moy_trans_donn_sender))
f1.write("\n")
f1.write("moyenne de donnee transfere receiver en MBytes: "+str(moy_trans_donn_receiver))
####################################################################################

workbook.close()
####################################################################################