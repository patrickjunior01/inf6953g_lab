#!/bin/bash

#Install required benchmark tools
./install_bench_tools.sh

#Run each benchmark 5 times and compress the results
for i in 1 2 3 4 5
do
   sleep 5m
   python ./run_benchmarks.py

   tar -zcvf "/tmp/BenchResult_$i.tar.gz" /tmp/BenchResult
   rm -rfv /tmp/BenchResult/
done
