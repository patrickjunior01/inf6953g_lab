import subprocess
import sys
import getpass

iteration = 5  # number of executions
login=getpass.getuser() ## To get the current user's username
output_file = "/home/"+login+"/output_redis.txt" 
result_file= "/home/"+login+"/score_execution_redis.txt" 
cmd_redis = "redis-benchmark -q -n 100000 -P 16"
print ("############ START ############")

for k in range(iteration):
	f = open(output_file,'wb')
	sys.stdout = f
	
	subprocess.check_call(cmd_redis,shell=True, stdout=f)  #Execution of iperf3
	sys.stdout = sys.__stdout__
	f.close()
####################################################################################

####################################################################################
	f =open(output_file,"r")
	f1 = open(result_file,'a') # fichier qui contient les resultats de tous les executions
	f1.write ("Execution"+str(k+1)+": " + "\n") 
	for line in f:
		tempList = line.split();
		if (len(tempList)!=0 and (tempList[0]) =="SET:" and (tempList[len(tempList)-1])=="second"):
			#f1.write("Debut Execution"+str(k))
			#f1.write("\n\n")
			for j in tempList:
				f1.write(j+" ")
			#f1.write("\n")
		
		if (len(tempList)!=0 and (tempList[0]) =="GET:" and (tempList[len(tempList)-1])=="second"):
			#f1.write("Debut Execution"+str(k))
			f1.write("\n")
			for j in tempList:
				f1.write(j+" ")
			#f1.write("\n\n")
			
		if (len(tempList)!=0 and (tempList[0]) =="LPUSH:" and (tempList[len(tempList)-1])=="second"):
			#f1.write("Debut Execution"+str(k))
			f1.write("\n")
			for j in tempList:
				f1.write(j+" ")
			f1.write("\n\n")
f.close()
sys.stdout = sys.__stdout__
print ("############ The execution of Redis is now terminated ############")