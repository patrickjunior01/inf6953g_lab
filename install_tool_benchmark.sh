#!/bin/bash
tput clear

#Updates fetching and Installation
echo -e '\E[39;46m'"\033[1mInstalling Updates...\033[0m"
apt-get -y update
apt-get -y dist-upgrade

#UnixBench Installation
echo -e '\E[37;44m'"\033[1m\nInstalling UnixBench...\033[0m"
apt-get -y install libx11-dev libgl1-mesa-dev libxext-dev perl perl-modules build-essential make
apt-get -y install gcc
wget -N http://byte-unixbench.googlecode.com/files/UnixBench5.1.3.tgz
tar -xvf UnixBench5.1.3.tgz 
cd UnixBench5.1.3
make

#IOPing Installation
echo -e '\E[37;44m'"\033[1m\nInstalling IOPing...\033[0m"
apt-get -y install ioping

#Redis Installation
echo -e '\E[37;44m'"\033[1m\nInstalling Redis...\033[0m"
sudo apt-get -y install build-essential
wget http://download.redis.io/releases/redis-stable.tar.gz
tar xzf redis-stable.tar.gz
mv redis-stable /etc
cd /etc/redis-stable
make distclean
make
apt-get -y install tcl8.5 dejagnu 
make install
cd utils ./install_server.sh

#DBench, SysBench, HardInfo, fio, gtkperf and Phoronix Test Suite Installation
echo -e '\E[37;44m'"\033[1m\nInstalling DBench, SysBench, HardInfo, FIO, GTKPerf & Phoronix...\033[0m"
apt-get -y install dbench sysbench hardinfo fio gtkperf phoronix-test-suite

echo -e '\E[34;43m'"\033[1m\nInstalling has been completed.\033[0m"

#Iperf Installation
echo -e '\E[37;44m'"\033[1m\nInstalling Iperf...\033[0m"
sudo apt-get -y install git gcc make
sudo git clone https://github.com/esnet/iperf
cd iperf
sudo ./configure
sudo make
sudo make install
sudo ldconfig

echo -e '\E[34;43m'"\033[1m\nInstalling has been completed.\033[0m"

