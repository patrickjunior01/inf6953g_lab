#!/usr/bin/env python3

############ SCRIPT FOR LAUNCHING UNIXBENCH ############ 

import os
import subprocess
import sys
import getpass

login=getpass.getuser() ## To get the current user's username
unixbench_dir="/home/"+login+"/UnixBench"
output_file="/home/"+login+"/output_unixbench.txt"
result_file="/home/"+login+"/score_execution_unixbench.txt"
iteration = 5
average = 0
os.chdir(unixbench_dir) #We change the directory to launch the program

print ("############ START ############")

for i in range(iteration):
	f = open(output_file,'wb')
	subprocess.check_call("./Run", stdout=f) #Execution of UnixBench with the redirection of the output to the file output_file.txt
	sys.stdout = sys.__stdout__ ##default value of the standard output

	## Now we put the score in the file result_file
	f1 = open(output_file)
	f = open(result_file,'a')
	sys.stdout = f
	for line in f1:
		fields = line.strip().split(' ')
		for j in range(len(fields)): 
			if fields[j] == "Score" : 
				print ("Execution"+str(i+1)+": " +fields[len(fields)-1]+ "\n") 
				average += float(fields[len(fields)-1])
	f1.close()
average = average / iteration
print ("Average of the executions: " +str(average))
f.close()
sys.stdout = sys.__stdout__
print ("############ The execution of UnixBench is now terminated ############")